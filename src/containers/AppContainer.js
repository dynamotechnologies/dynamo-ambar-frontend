import React, { Component, PropTypes } from 'react'
import { Router } from 'react-router'
import { Provider } from 'react-redux'
import injectTapEventPlugin from 'react-tap-event-plugin'

import {cyan500} from 'material-ui/styles/colors'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'

const currentTheme = getMuiTheme({
  fontFamily: 'Roboto, sans-serif',
  palette: {
    primary1Color: '#345995',
    primary2Color: '#0E2239',
    primary3Color: '#EAC435',
    accent1Color: '#03CEA4',
    accent3Color: '#03CEA4',
    accent3Color: '#03CEA4',
  },
});

class AppContainer extends Component {
  static propTypes = {
    history: PropTypes.object.isRequired,
    routes: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired
  }

  static childContextTypes =
  {
    muiTheme: React.PropTypes.object
  }

  constructor(args) {
    try{
      injectTapEventPlugin()
    }
    catch(exc) { console.error(exc) }
    super(args)
  }

  getChildContext() {
    return {
      muiTheme: getMuiTheme()
    }
  }

  render() {
    const { history, routes, store } = this.props

    return (
      <Provider store={store}>
        <MuiThemeProvider muiTheme={currentTheme}>
          <div style={{ height: '100%' }}>
            <Router history={history} children={routes} />
          </div>
        </MuiThemeProvider>
      </Provider>
    )
  }
}

export default AppContainer
