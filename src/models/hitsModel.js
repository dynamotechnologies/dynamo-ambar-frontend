import moment from 'moment'
import { dates, urls } from 'utils/'

export const fromApi = (resp) => {
    let hits = new Map()
    resp.hits.forEach((hit) => {
        hits.set(hit.file_id, {
            ...hit,
            fetching: false
        })
    })
    return hits
}

export const contentHighlightFromApi = (resp) => {
    return resp.highlight
}

export const getHit = (state, fileId) => {
    const hit = state.hits.get(fileId)
    return hit
}

export const updateHits = (state, fileId, hit) => {
    const newState = { ...state, hits: new Map(state.hits) }
    newState.hits.set(fileId, hit)
    return newState
}

export const getTitlesFromHits = (resp) => {
    if (!resp || !resp.hits || !resp.hits.hits) return [];
    let titles = [];
    titles = resp.hits.hits.map((hit) => {
      return {
        id: hit._id,
        name: hit._source.name
      }
    })

    return titles;
}

export const getHitsFromSavedResults = (resp) => {
    if (!resp || !resp._source || !resp._source.results) return [];
    return resp._source.results;
}
