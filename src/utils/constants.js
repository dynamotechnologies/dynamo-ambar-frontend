export const errorMessage = 'Oops... Something went wrong. Please reload the page'
export const FOLDER_VIEW = 'FOLDER_VIEW'
export const DETAILED_VIEW = 'DETAILED_VIEW'
export const TABLE_VIEW = 'TABLE_VIEW'
export const STATISTICS_VIEW = 'STATISTICS_VIEW'
export const WORKFLOW_VIEW = 'WORKFLOW_VIEW'

export const LOGIN_EMAIL = 'evan.lee@oig.hhs.gov';
export const LOGIN_TOKEN = 'test';
export const LOGIN_PASSWORD = 'password123';

const CORS_ANYWHERE = 'http://localhost:8000/';
// export const DYNAMO_HOST = CORS_ANYWHERE+'localhost:9200';
// export const DYNAMO_HOST = 'http://localhost:9200';
export const DYNAMO_HOST = 'https://search-hhs-oig-poc-public-25ivdk4pnsihtm4owkpskmuj2y.us-east-1.es.amazonaws.com';
export const SAVE_SEARCH_RESULTS_PATH = 'workflow/search-results-test'
