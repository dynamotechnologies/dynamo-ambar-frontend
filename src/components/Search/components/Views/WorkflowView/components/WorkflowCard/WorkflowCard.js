import React, { Component } from 'react'
import { LoadingIndicator, TagsInput } from 'components/BasicComponents'
import { Card, CardActions, CardText, CardTitle } from 'material-ui/Card'
import MediaQuery from 'react-responsive'
import Paper from 'material-ui/Paper'
import { Divider, FlatButton } from 'material-ui'
import FileDownloadIcon from 'material-ui/svg-icons/file/file-download'
import BlockIcon from 'material-ui/svg-icons/content/block'
import RemoveIcon from 'material-ui/svg-icons/content/remove'
import TextPreviewIcon from 'material-ui/svg-icons/action/subject'
import DeleteIcon from 'material-ui/svg-icons/action/delete'
import UndoIcon from 'material-ui/svg-icons/content/undo'
import WorkflowCardHeader from './components/WorkflowCardHeader'
import { files } from 'utils/'

import classes from './WorkflowCard.scss'

class WorkflowCard extends Component {
    startLoadingHighlight() {
        const { searchQuery, hit: { file_id: fileId }, loadHighlight } = this.props
        loadHighlight(fileId, searchQuery)
    }

    render() {
        const {
            hit: {
                fetching: fetching,
                meta: meta,
                content: content,
                sha256: sha256,
                tags: tags,
                file_id: fileId,
                isHidden: isHidden,
                hidden_mark: hidden_mark,
                isCulled,
                isRedacted,
                isNonResponsive
            },
            allTags,
            thumbnailUri,
            downloadUri,
            searchQuery,
            loadHighlight,
            performSearchByAuthor,
            performSearchByPathToFile,
            toggleImagePreview,
            openTextPreview,
            addTagToFile,
            removeTagFromFile,
            performSearchByTag,
            hideFile,
            showFile,
            localization,
            preserveOriginals,
            cullFile,
            redactFile,
            markFileAsNonResponsive,
            restoreFile
        } = this.props

        const contentHighlight = content && content.highlight && content.highlight.text ? content.highlight.text : undefined

        return (
            <Paper zDepth={1} className={classes.searchResultRowCard}>
                <Card>
                    <WorkflowCardHeader
                        searchQuery={searchQuery}
                        meta={meta}
                        content={content}
                        performSearchByPathToFile={performSearchByPathToFile}
                        performSearchByAuthor={performSearchByAuthor}
                        localization={localization}
                    />
                    {!isHidden && <div>
                        <TagsInput
                            tags={tags}
                            onAddTag={(tagType, tagName) => addTagToFile(fileId, tagType, tagName)}
                            onRemoveTag={(tagType, tagName) => removeTagFromFile(fileId, tagType, tagName)}
                            performSearchByTag={performSearchByTag}
                            suggestions={allTags.map(t => t.name)}
                        />
                        <div className={classes.searchResultRowCardTextContainer}>
                            <div className={classes.searchResultRowCardTextDiv}>
                                {fetching && <CardText>
                                    <LoadingIndicator />
                                </CardText>
                                }
                                {!fetching && !contentHighlight &&
                                    <CardText onMouseEnter={() => this.startLoadingHighlight()}>
                                        <span className={classes.blurred}>Если у общества нет цветовой дифференциации штанов - то у общества</span><br />
                                        <span className={classes.blurred}>нет цели, а если нет цели - то...</span>
                                    </CardText>
                                }
                                {!fetching && contentHighlight && contentHighlight.map((hl, idx) =>
                                    <CardText key={idx}
                                        className={idx != contentHighlight.length - 1 ? classes.searchResultRowCardTextWithBorder : undefined}
                                        dangerouslySetInnerHTML={{ __html: hl }}
                                    />)
                                }
                            </div>
                            {!fetching && contentHighlight && content.thumb_available &&
                                <MediaQuery query='(min-width: 1024px)'>
                                    <div className={classes.searchResultRowCardTextThumbnailContainer} >
                                        <img onTouchTap={() => { toggleImagePreview(thumbnailUri) }}
                                            className={classes.searchResultRowCardTextThumbnailImage}
                                            src={thumbnailUri} />
                                    </div>
                                </MediaQuery>
                            }
                        </div>
                    </div>}
                    <CardActions className={classes.searchResultRowCardFooter}>
                        <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                            {!isHidden && <div>
                                {preserveOriginals && <FlatButton
                                    icon={<FileDownloadIcon />}
                                    label={localization.searchPage.downloadLabel}
                                    title={localization.searchPage.downloadDescriptionLabel}
                                    primary={true}
                                    onTouchTap={() => { window.open(downloadUri) }}
                                />}
                                <FlatButton
                                    icon={<TextPreviewIcon />}
                                    label={localization.searchPage.previewLabel}
                                    title={localization.searchPage.previewDescriptionLabel}
                                    disabled={!files.doesFileContainText(meta)}
                                    primary={true}
                                    onTouchTap={() => openTextPreview(fileId)}
                                />
                            </div>}
                            {isHidden && <div>
                                {isCulled &&
                                    <FlatButton
                                      icon={<DeleteIcon />}
                                      label={'Culled'}
                                      title={'Culled result'}
                                      secondary={true}
                                      disabled={true}
                                    />
                                }
                                {isRedacted &&
                                    <FlatButton
                                      icon={<BlockIcon />}
                                      label={'Redacted'}
                                      title={'Redacted result'}
                                      secondary={true}
                                      disabled={true}
                                />
                              }
                                {isNonResponsive &&
                                    <FlatButton
                                      icon={<RemoveIcon />}
                                      label={'Non-Responsive'}
                                      title={'Mark result as non-responsive'}
                                      secondary={true}
                                      disabled={true}
                                    />
                                }
                            </div>}
                            <div>

                              { !hidden_mark &&
                                <FlatButton
                                  icon={<DeleteIcon />}
                                  label={'Cull'}
                                  title={'Cull result'}
                                  primary={true}
                                  onTouchTap={() => cullFile(fileId)}
                                />
                              }
                              { !hidden_mark &&
                                <FlatButton
                                  icon={<BlockIcon />}
                                  label={'Redact'}
                                  title={'Redact result'}
                                  primary={true}
                                  onTouchTap={() => redactFile(fileId)}
                                />
                              }
                              { !hidden_mark &&
                                <FlatButton
                                  icon={<RemoveIcon />}
                                  label={'Non-Responsive'}
                                  title={'Mark result as non-responsive'}
                                  primary={true}
                                  onTouchTap={() => markFileAsNonResponsive(fileId)}
                                />
                              }

                                {(isHidden || hidden_mark) && <FlatButton
                                    icon={<UndoIcon />}
                                    label={localization.searchPage.restoreLabel}
                                    title={localization.searchPage.restoreDescriptionLabel}
                                    primary={true}
                                    onTouchTap={() => restoreFile(fileId)}
                                />}

                            </div>
                        </div>}
                    </CardActions>
                </Card>
            </Paper>
        )
    }
}


WorkflowCard.propTypes = {
    hit: React.PropTypes.object.isRequired,
    allTags: React.PropTypes.array.isRequired,
    searchQuery: React.PropTypes.string.isRequired,
    thumbnailUri: React.PropTypes.string.isRequired,
    downloadUri: React.PropTypes.string.isRequired,
    loadHighlight: React.PropTypes.func.isRequired,
    performSearchByAuthor: React.PropTypes.func.isRequired,
    performSearchByPathToFile: React.PropTypes.func.isRequired,
    toggleImagePreview: React.PropTypes.func.isRequired,
    openTextPreview: React.PropTypes.func.isRequired,
    addTagToFile: React.PropTypes.func.isRequired,
    removeTagFromFile: React.PropTypes.func.isRequired,
    performSearchByTag: React.PropTypes.func.isRequired,
    hideFile: React.PropTypes.func.isRequired,
    showFile: React.PropTypes.func.isRequired,
    localization: React.PropTypes.object.isRequired,
    preserveOriginals: React.PropTypes.bool.isRequired
}

export default WorkflowCard




