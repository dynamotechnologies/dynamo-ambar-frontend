import React, { Component } from 'react'
import FlatButton from 'material-ui/FlatButton'
import classes from './WorkflowHeader.scss'
import { Card, CardActions, CardHeader, CardText, CardTitle } from 'material-ui/Card'
import Paper from 'material-ui/Paper'
import TextField from 'material-ui/TextField'
import MenuItem from 'material-ui/MenuItem'
import SelectField from 'material-ui/SelectField';


class WorkflowHeader extends Component {

    constructor(props) {
      super(props)
      this.state = {value: null}
    }

  componentWillReceiveProps = (nextProps) => {
    if (!nextProps.selectedSavedSearchResult.name) this.setState({value: null})
  }

    render() {
      const title = 'Manage Search Results'
      const description = 'description';

      const {
        saveSearchResults,
        updateSaveName,
        saveName,
        savedSearchResultTitles,
        selectedSavedSearchResult,
        updateSelectedSavedSearchResult,
        getSavedSearchResult,
        updateSearchResult,
        hits
      } = this.props;

      const handleChange = (event, index, value) => {
        if (value === null) return;

        const selection = savedSearchResultTitles[index - 1];
        updateSelectedSavedSearchResult(selection)
        getSavedSearchResult(selection)
      }

      return (
        <Paper zDepth={1} className={classes.headerCard} >
          <Card >
            <CardHeader
              className={classes.emptyCardTitle}
              title={<span className={classes.emptyCardHeaderTitle}>{title}</span>}
            />
            <CardText>

              <div style={{ display: 'flex', justifyContent: 'space-between', width: '100%' }}>
                <div>
                  <TextField
                    value={saveName}
                    floatingLabelText="Save new result set"
                    onChange={(event, value) => updateSaveName(value)}
                  />
                  <FlatButton
                    label="Create"
                    primary={true}
                    onTouchTap={() => saveSearchResults()}
                    disabled={!hits || hits.size === 0 || saveName === '' || saveName === selectedSavedSearchResult.name}
                  />
                  <FlatButton
                    label="Update"
                    primary={true}
                    onTouchTap={() => updateSearchResult()}
                    disabled={!selectedSavedSearchResult.name}
                  />
                </div>
                <div>
                  <SelectField
                    maxHeight={300}
                    value={selectedSavedSearchResult.id}
                    onChange={handleChange}
                    floatingLabelText="Load saved result set"
                    selectedMenuItemStyle={{ color: 'rgb(52, 89, 149)' }}
                  >
                    <MenuItem key={0} value={null} primaryText=''/>
                    {
                      savedSearchResultTitles.map(result =>
                        <MenuItem key={result.id} value={result.id} primaryText={result.name}/>
                      )
                    }
                  </SelectField>
                </div>
              </div>
            </CardText>
          </Card>
        </Paper>
      )
    }

}

WorkflowHeader.propTypes = {
  saveSearchResults: React.PropTypes.func.isRequired,
  updateSaveName: React.PropTypes.func.isRequired,
  saveName: React.PropTypes.string.isRequired,
  savedSearchResultTitles: React.PropTypes.array.isRequired,
  selectedSavedSearchResult: React.PropTypes.object.isRequired,
  updateSelectedSavedSearchResult: React.PropTypes.func.isRequired,
  getSavedSearchResult: React.PropTypes.func.isRequired
}

export default WorkflowHeader




