import React, { Component } from 'react'

import EmptySearchResultsContainer from 'routes/SearchPage/containers/EmptySearchResultsContainer'
import WorkflowCard from './components/WorkflowCard'
import WorkflowHeader from './components/WorkflowHeader'
import classes from './WorkflowView.scss'

class WorkflowView extends Component {
    componentDidMount() {
        const {
            search,
            searchQuery,
            getSavedSearchResultTitles
        } = this.props

        getSavedSearchResultTitles();
        search(0, searchQuery)
    }

    render() {
        const {
            hits,
            searchQuery,
            urls,
            saveSearchResults,
            updateSaveName,
            saveName,
            savedSearchResultTitles,
            updateSelectedSavedSearchResult,
            selectedSavedSearchResult,
            getSavedSearchResult,
            updateSearchResult
        } = this.props

        return (
              <div className='pageContainer'>
                  <WorkflowHeader
                      saveSearchResults={saveSearchResults}
                      updateSaveName={updateSaveName}
                      saveName={saveName}
                      savedSearchResultTitles={savedSearchResultTitles}
                      updateSelectedSavedSearchResult={updateSelectedSavedSearchResult}
                      selectedSavedSearchResult={selectedSavedSearchResult}
                      getSavedSearchResult={getSavedSearchResult}
                      updateSearchResult={updateSearchResult}
                      hits={hits}
                  />
                <div style={{marginTop: '180px'}}/>
                {hits && hits.size > 0 &&
                  <div>
                    {Array.from(hits.values()).map((hit, idx) =>
                      <WorkflowCard
                        key={hit.file_id}
                        hit={hit}
                        thumbnailUri={urls.ambarWebApiGetThumbnail(hit.sha256)}
                        downloadUri={urls.ambarWebApiGetFile(hit.meta.download_uri)}
                        {...this.props}
                      />
                    )}
                  </div>
                }
              </div>
        )
    }
}

WorkflowView.propTypes = {
    hits: React.PropTypes.object.isRequired,
    search: React.PropTypes.func.isRequired,
    searchQuery: React.PropTypes.string.isRequired,
    urls: React.PropTypes.object.isRequired,
    saveSearchResults: React.PropTypes.func.isRequired,
    updateSaveName: React.PropTypes.func.isRequired,
    saveName: React.PropTypes.string.isRequired,
    getSavedSearchResultTitles: React.PropTypes.func.isRequired,
    savedSearchResultTitles: React.PropTypes.array.isRequired,
    selectedSavedSearchResult: React.PropTypes.object.isRequired,
    updateSelectedSavedSearchResult: React.PropTypes.func.isRequired,
    getSavedSearchResult: React.PropTypes.func.isRequired,
    updateSearchResult: React.PropTypes.func.isRequired
}

export default WorkflowView
