import { connect } from 'react-redux'
import { stateValueExtractor } from 'utils/'

import { openTextPreview } from '../modules/TextPreviewModal'
import { toggleImagePreview } from '../modules/ImagePreview'
import { addTagToFile, removeTagFromFile } from '../modules/TagsReducer'
import { hideFile, showFile } from '../modules/FileVisibilityReducer'
import { loadHighlight } from '../modules/DetailedView'
import {
  cullFile,
  redactFile,
  markFileAsNonResponsive,
  restoreFile,
} from '../modules/WorkflowView'

import {
    performSearchByPathToFile,
    performSearchByAuthor,
    performSearchByQuery,
    performSearchByTag,
    search,
    saveSearchResults,
    updateSaveName,
    getSavedSearchResultTitles,
    updateSelectedSavedSearchResult,
    getSavedSearchResult,
    updateSearchResult
} from '../modules/SearchReducer'

import WorkflowView from 'components/Search/components/Views/WorkflowView'

const mapDispatchToProps = {
    search,
    performSearchByPathToFile,
    performSearchByAuthor,
    performSearchByQuery,
    performSearchByTag,
    toggleImagePreview,
    addTagToFile,
    removeTagFromFile,
    hideFile,
    showFile,
    openTextPreview,
    loadHighlight,
    cullFile,
    redactFile,
    markFileAsNonResponsive,
    restoreFile,
    saveSearchResults,
    updateSaveName,
    getSavedSearchResultTitles,
    updateSelectedSavedSearchResult,
    getSavedSearchResult,
    updateSearchResult
}

const mapStateToProps = (state, ownProps) => {
    return ({
        hits: state['searchPage'].hits,
        localization: stateValueExtractor.getLocalization(state),
        urls: stateValueExtractor.getUrls(state),
        allTags: state['searchPage'].tags,
        searchQuery: state['searchPage'].searchQuery,
        preserveOriginals: state['core'].preserveOriginals,
        saveName: state['searchPage'].saveName,
        savedSearchResultTitles: state['searchPage'].savedSearchResultTitles,
        selectedSavedSearchResult: state['searchPage'].selectedSavedSearchResult,
    })
}

export default connect(mapStateToProps, mapDispatchToProps)(WorkflowView)
