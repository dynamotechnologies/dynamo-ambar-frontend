import { stateValueExtractor } from 'utils/'
import { hitsModel } from 'models/'
import { analytics } from 'utils'
import { handleError } from 'routes/CoreLayout/modules/CoreLayout'
import { startLoadingIndicator, stopLoadingIndicator } from 'routes/MainLayout/modules/MainLayout'

export const START_STOP_HIGHLIGHT_LOADING = 'WORKFLOW_VIEW.START_STOP_HIGHLIGHT_LOADING'
export const SET_CONTENT_HIGHLIGHT = 'WORKFLOW_VIEW.SET_CONTENT_HIGHLIGHT'
export const TOGGLE_IS_CULLED_FILE = 'WORKFLOW_VIEW.TOGGLE_IS_CULLED_FILE';
export const TOGGLE_IS_REDACTED_FILE = 'WORKFLOW_VIEW.TOGGLE_IS_REDACTED_FILE';
export const TOGGLE_IS_NON_RESPONSIVE_FILE = 'WORKFLOW_VIEW.TOGGLE_IS_NON_RESPONSIVE_FILE';
export const RESTORE_FILE = 'WORKFLOW_VIEW.RESTORE_FILE';

export const cullFile = (fileId) => {
  return (dispatch, getState) => {
    dispatch(toggleIsCulledFile(fileId, true))
  }
}

export const redactFile = (fileId) => {
  return (dispatch, getState) => {
    dispatch(toggleIsRedactedFile(fileId, true))
  }
}

export const markFileAsNonResponsive = (fileId) => {
  return (dispatch, getState) => {
    dispatch(toggleIsNonResponsiveFile(fileId, true))
  }
}

export const restoreFile = (fileId) => {
  return (dispatch, getState) => {
    dispatch(setRestoreFile(fileId))
  }
}

const toggleIsCulledFile = (fileId, value) => {
  return {
    type: TOGGLE_IS_CULLED_FILE,
    fileId: fileId,
    value: value
  }
}

const toggleIsRedactedFile = (fileId, value) => {
  return {
    type: TOGGLE_IS_REDACTED_FILE,
    fileId: fileId,
    value: value
  }
}

const toggleIsNonResponsiveFile = (fileId, value) => {
  return {
    type: TOGGLE_IS_NON_RESPONSIVE_FILE,
    fileId: fileId,
    value: value
  }
}

const setRestoreFile = (fileId) => {
  return {
    type: RESTORE_FILE,
    fileId: fileId,
    value: false
  }
}

const getHit = (state, fileId) => {
  const hit = state.hits.get(fileId)
  return hit
}

const updateHits = (state, fileId, hit) => {
  const newState = { ...state, hits: new Map(state.hits) }
  newState.hits.set(fileId, hit)
  return newState
}

export const ACTION_HANDLERS = {
  [TOGGLE_IS_CULLED_FILE]: (state, action) => {
    const oldHit = getHit(state, action.fileId)
    const hit = { ...oldHit, isCulled: action.value, isHidden: action.value, hidden_mark: action.value ? {} : null }
    return updateHits(state, action.fileId, hit)
  },
  [TOGGLE_IS_REDACTED_FILE]: (state, action) => {
    const oldHit = getHit(state, action.fileId)
    const hit = { ...oldHit, isRedacted: action.value, isHidden: action.value, hidden_mark: action.value ? {} : null }
    return updateHits(state, action.fileId, hit)
  },
  [TOGGLE_IS_NON_RESPONSIVE_FILE]: (state, action) => {
    const oldHit = getHit(state, action.fileId)
    const hit = { ...oldHit, isNonResponsive: action.value, isHidden: action.value, hidden_mark: action.value ? {} : null }
    return updateHits(state, action.fileId, hit)
  },
  [RESTORE_FILE]: (state, action) => {
    const oldHit = getHit(state, action.fileId)
    const hit = { ...oldHit, isNonResponsive: action.value, isCulled: action.value, isRedacted: action.value,
      isHidden: action.value, hidden_mark: action.value ? {} : null }
    return updateHits(state, action.fileId, hit)
  },
}
